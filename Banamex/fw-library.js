var antiClickjackScript = document.createElement('script');
var antiClickjackStyle = document.createElement('style');
antiClickjackStyle.id = 'antiClickjack';
antiClickjackStyle.innerHTML = 'body{display:none !important;}';
antiClickjackScript.type = 'text/javascript';
antiClickjackScript.innerHTML = 'if (self === top) { var antiClickjack = document.getElementById("antiClickjack"); antiClickjack.parentNode.removeChild(antiClickjack);} else { top.location = self.location;}';
document.getElementsByTagName('head')[0].appendChild(antiClickjackStyle);
document.getElementsByTagName('head')[0].appendChild(antiClickjackScript);


function FWConfiguration() {
	
	this.UAT = 'https://kio.banamex.com/frameworkUAT';
	this.PROD = 'https://kio.banamex.com/framework';
	this.SERVICE = '/query';
	
	this.dataFormater = function (data) { 
		return data;
	}
}
	FWConfiguration.prototype.setProd = function(urlProd) {
		this.PROD = urlProd;
	}

	FWConfiguration.prototype.setUat = function(urlUat) {
		this.UAT = urlUat;
	}

	FWConfiguration.prototype.getURL = function(service) {
		var url =
			window.location.href.indexOf('www.banamex.com') == -1
			    ? this.UAT+service
			    : this.PROD+service;

		return url;
	}

	FWConfiguration.prototype.getURLService = function() {
		return this.getURL(this.SERVICE);
	}

	FWConfiguration.prototype.setDataFormater = function(functionBody) {
		this.dataFormater = functionBody;
	}

function FWConfigurable(url) {

    this.config = new FWConfiguration();
    if (url && (url.hasOwnProperty('UAT') || url.hasOwnProperty('PROD'))) {
        this.config.setProd(url.PROD||url.UAT);
        this.config.setUat(url.UAT||url.PROD);
    }else if( url ){
        this.config.setProd(url);
        this.config.setUat(url);
    }
}
    

    FWConfigurable.prototype.getConfig = function() {
        return this.config;
    }

    FWConfigurable.prototype.setConfig = function(config) {
        this.config = config;
    }

    FWConfigurable.prototype.getFetchBody = function (data) {
        return {
            method: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            body: JSON.stringify(data),
        };
    }

    FWConfigurable.prototype.doFetch = function(url, fetchBody) {
        return fetch(url, fetchBody);
    }

    FWConfigurable.prototype.getPromise = function(objectBody) {
        var _this = this;
        return new Promise(function(resolve, reject){
            fetch(_this.config.getURLService(), _this.getFetchBody(objectBody))
                .then(function(response) {
                    if (response.ok) {
                        return response.json();
                    } else {
                        reject('Error en petición.');
                    }
                })
                .then(function(json){
                    if (json.Error) {
                        reject(json.Error);
                    } else {
                        if (Array.isArray(json.data)) {
                            json.data = json.data.map(function(obj){
                                    return _this.config.dataFormater(obj);
                                }
                            );
                        } else {
                            json.data = _this.config.dataFormater(json.data);
                        }
                        resolve(json);
                    }
                })
                .catch(function(err){
                    reject('Error es: ' + err);
                });
        });
    }


function  CAT(url) {
    
        if(url){
            FWConfigurable.call(this,url);
        }else{
            FWConfigurable.call(this,{
                UAT:'https://uat.proyectos.citibanamex.com/framework',
                PROD:'https://proyectos.citibanamex.com/framework'
            });
        }
        this.site = '7';
        this.entity = 'pt_cats';
        this.config.setDataFormater(function(data) {
            data.id = data.prod;
            return data;
        });
    }
    CAT.prototype = Object.create(FWConfigurable.prototype);
    CAT.prototype.constructor = CAT;


    CAT.prototype.getCatsByObjectBody = function(objectBody) {
        objectBody.site = this.site;
        objectBody.entity = this.entity;
        return this.getPromise(objectBody);
    }

    CAT.prototype.getById = function(id, page, pageSize) {
        var objectBody = {
            operation: 'getEntityById',
            idExpression: 'id='+id,
        };
        if (page) {
            objectBody.page = page.toString();
        }
        if (pageSize) {
            objectBody.page_size = pageSize.toString();
        }
        return this.getCatsByObjectBody(objectBody);
    }

    CAT.prototype.getAll= function(page, pageSize) {
        var objectBody = {
            operation: 'getAll',
        };
        if (page) {
            objectBody.page = page.toString();
        }
        if (pageSize) {
            objectBody.page_size = pageSize.toString();
        }
        return this.getCatsByObjectBody(objectBody);
    }

    CAT.prototype.getByOptions = function(options, page, pageSize, logicalOperator) {
        var objectBody = {
            operation: 'getEntitiesByOptions',
            options: options,
        };
        if (page) {
            objectBody.page = page.toString();
        }
        if (pageSize) {
            objectBody.page_size = pageSize.toString();
        }
        if (logicalOperator) {
            objectBody.logicalOperator = logicalOperator.toString();
        }
        return this.getCatsByObjectBody(objectBody);
    }


    function getInstanceCredito(nombreMicrositio, url) {
        return new Promise(function(resolve, reject){
            try {
                function Credito (nombreMicrositio, url){
                    if(url){
                        FWConfigurable.call(this,url);
                    }else{
                        FWConfigurable.call(this,{
                            UAT:'https://uat.proyectos.citibanamex.com/framework',
                            PROD:'https://proyectos.citibanamex.com/framework'
                        });
                    }
                    this.Extra = '';
                    this.nombreMicrositio = nombreMicrositio;
                }
                Credito.prototype = Object.create(FWConfigurable.prototype);
                Credito.prototype.constructor = Credito;
                $.getScript('/assets/js/fw-modules/Credito.js').done(function(){
                    buildCredito(Credito);
                    resolve(new Credito(nombreMicrositio, url));
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    function getInstanceCompromiso(url) {
        return new Promise(function(resolve, reject){
            try {
                function Compromiso (url){
                    if(url){
                        FWConfigurable.call(this,url);
                    }else{
                        FWConfigurable.call(this,{
                            UAT:'https://uat.proyectos.citibanamex.com/framework',
                            PROD:'https://proyectos.citibanamex.com/framework'
                        });
                    }
                    this.site = '10';
                    this.arrysCONDUSEF = [];
                }
                Compromiso.prototype = Object.create(FWConfigurable.prototype);
                Compromiso.prototype.constructor = Compromiso;
                $.getScript('/assets/js/fw-modules/Compromiso.js').done(function(){
                    buildCompromiso(Compromiso);
                    resolve(new Compromiso(url));
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    function getInstanceMicrositios(nombreMicrositio, url) {
        return new Promise(function(resolve, reject){
            try {
                function Micrositios (nombreMicrositio, url){
                    if(url){
                        FWConfigurable.call(this,url);
                    }else{
                        FWConfigurable.call(this,{
                            UAT:'https://uat.proyectos.citibanamex.com/framework',
                            PROD:'https://proyectos.citibanamex.com/framework'
                        });
                    }
                    this.Extra = '';
                    this.nombreMicrositio = nombreMicrositio;
                }
                Micrositios.prototype = Object.create(FWConfigurable.prototype);
                Micrositios.prototype.constructor = Micrositios;
                $.getScript('/assets/js/fw-modules/Micrositios.js').done(function(){
                    buildMicrositios(Micrositios);
                    resolve(new Micrositios(nombreMicrositio, url));
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    function getInstancePromociones(url) {
        return new Promise(function(resolve, reject){
            try {
                function Promociones(url){
                    if(url){
                        FWConfigurable.call(this,url);
                    }else{
                        FWConfigurable.call(this,{
                            UAT:'https://uat.proyectos.citibanamex.com/framework',
                            PROD:'https://proyectos.citibanamex.com/framework'
                        });
                    }
                    this.site = '5';
                }
                Promociones.prototype = Object.create(FWConfigurable.prototype);
                Promociones.prototype.constructor = Promociones;
                $.getScript('/assets/js/fw-modules/Promociones.js').done(function(){
                    buildPromociones(Promociones);
                    resolve(new Promociones(url));
                });
            } catch (error) {
                reject(error);
            }
        });
    }